package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/juanlubel/quick.notes/core"
	"gitlab.com/juanlubel/quick.notes/notes"
)

func main() {
	r := gin.Default()

	db, _ := database.Connect()

	/* migrations */
	notes.MigrateNotes(db)

	v1 := r.Group("/api")
	notes.Manage(v1.Group("/notes"))
	err := r.Run()
	if err != nil {
		return
	} // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
