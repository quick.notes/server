package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	*gorm.DB
}

var DB *gorm.DB

func Connect() (*gorm.DB, error) {
	dsn := "host=postgres user=postgres dbname=notes password=supersecret port=5432 sslmode=disable TimeZone=Europe/Madrid"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	db.DB()
	DB = db
	return DB, err
}

func GetDb() *gorm.DB {
	return DB
}
