package notes

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateNote(c *gin.Context) {
	msg := NoteModel{}
	if err := c.BindJSON(&msg); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}
	err := msg.InsertNote()
	if err != nil {
		return
	}
}
