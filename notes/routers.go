package notes

import "github.com/gin-gonic/gin"

func Manage(router *gin.RouterGroup) {
	router.POST("/create", CreateNote)
}
