package notes

import (
	"fmt"
	database "gitlab.com/juanlubel/quick.notes/core"
	"gorm.io/gorm"
	"time"
)

type NoteModel struct {
	Msg       string    `json:"msg"`
	CreatedAt time.Time `json:"created-at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type NoteBodyReceiver struct {
	Msg string `json:"msg"`
}

func MigrateNotes(db *gorm.DB) {
	err := db.AutoMigrate(NoteModel{})
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (n *NoteModel) InsertNote() error {
	db := database.GetDb()
	err := db.Create(n).Error
	return err
}
